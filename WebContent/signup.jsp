<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
                <div class="errorMessages">
                    <ul>

                    </ul>
                </div>

            <form action="signup" method="post"><br />

            	<a href="./">ユーザー管理</a><br />

                <label for="account">アカウント</label>
                <input name="account" id="account" /> <br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br />

                <label for="password">確認用パスワード</label>
                <input name="password" type="password" id="password" /> <br />

                <label for="name">名前</label>
                <input name="name" id="name" /><br />

                <label for="branches">支社</label>
                <input name="branch_id" id="branch_id" /><br />

                <label for="department">部署</label>
                <input name="department_id" id="department_id" /> <br />

               <input type="submit" value="登録" /> <br />

                <br />
            </form>
            <div class="copyright">Copyright(c)Yu Nakahara</div>
        </div>
    </body>
</html>