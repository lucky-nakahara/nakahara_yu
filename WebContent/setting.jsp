<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${loginUser.account}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">




            <form action="setting" method="post"><br />
                <input name="id" value="${user.id}" id="id" type="hidden"/>

                <label for="account">アカウント名</label>
                <input name="account" id="account" /><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="password">確認用パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="name">名前</label>
                <input name="name" value="${user.name}" id="name"/><br />

                 <label for="branches">支社</label>
                <input name="branches" id="branches" /> <br />

                 <label for="department">部署</label>
                <input name="department" id="department" /> <br />



                <input type="submit" value="更新" /> <br />
                <a href="./">戻る</a>
            </form>

            <div class="copyright"> Copyright(c)Yu Nakahara</div>
        </div>
    </body>
</html>