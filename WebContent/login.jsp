<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
            <form action="login" method="post"><br />
                &emsp;&nbsp;<label for="accountOrEmail">アカウント&emsp;</label>
                <input name="accountOrEmail" id="accountOrEmail"/> <br />

                &emsp;<label for="password">パスワード&emsp;</label>
                <input name="password" type="password" id="password"/> <br />

                &emsp;<input type="submit" value="ログイン" /> <br />

            </form>
            <div class="copyright"> Copyright(c)YuNakahara</div>
        </div>
    </body>
</html>