<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>簡易Twitter</title>
    </head>
    <body>
        <div class="main-contents">
            <div class="header">

				<a href="message">新規投稿</a>

    			<a href="setting">ユーザー管理</a>

    			<a href="logout">ログアウト</a>

            </div>
            <div class="copyright"> Copyright(c)Yu Nakahara</div>
        </div>
    </body>
</html>