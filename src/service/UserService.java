package service;

import java.sql.Connection;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;
import utils.CloseableUtil;
import utils.DBUtil;

public class UserService {

    public void insert(User user) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            connection = DBUtil.getConnection();

            new UserDao().insert(connection, user);
            DBUtil.commit(connection);
        } catch (RuntimeException e) {
        	DBUtil.rollback(connection);
            throw e;
        } catch (Error e) {
        	DBUtil.rollback(connection);
            throw e;
        } finally {
        	CloseableUtil.close(connection);
        }
    }

    public User select(String account, String password) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(password);

            connection = DBUtil.getConnection();
            User user = new UserDao().select(connection, account, encPassword);
            DBUtil.commit(connection);

            return user;
        } catch (RuntimeException e) {
        	DBUtil.rollback(connection);
            throw e;
        } catch (Error e) {
        	DBUtil.rollback(connection);
            throw e;
        } finally {
        	CloseableUtil.close(connection);
        }
    }
}

