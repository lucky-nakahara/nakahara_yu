package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

    private int id;
    private String account;
    private String password;
    private String name;
    private String branchId;
    private String departmentId;
    private String isStopped;
    private Date createdDate;
    private Date updatedDate;

    public int getId() {
    	return id;
    }

    public void setId(int id) {
    	this.id = id;
    }

    public String getAccount() {
    	return account;
    }

    public void setAccount(String account) {
    	this.account = account;
    }

    public String getPassword() {
    	return password;
    }

    public void setPassword(String password) {
    	this.password = password;
    }

    public String getName() {
    	return name;
    }

    public void setName(String name) {
    	this.name = name;
    }

    public String getBranch_id() {
    	return branchId;
    }

    public void setBranch_id(String branch_id) {
    	this.branchId = branch_id;
    }

    public String getDepartment_id() {
    	return departmentId;
    }

    public void setDepartment_id(String department_id) {
    	this.departmentId = department_id;
    }

    public String getIs_stopped() {
    	return isStopped;
    }

    public void setIs_stopped(String is_stopped) {
    	this.isStopped = is_stopped;
    }

    public Date getCreatedDate() {
    	return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
    	return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
    	this.updatedDate = updatedDate;
    }
}